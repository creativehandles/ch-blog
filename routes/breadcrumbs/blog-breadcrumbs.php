<?php

// Dashboard > Blog
Breadcrumbs::for('admin.blog', function ($trail) {
//    $trail->parent('dashboard');
    $trail->push(__('general.Blog'), route('admin.blog'));
});

//Dashboard > Blog > New Article

Breadcrumbs::for('admin.createArticle', function ($trail) {
    $trail->parent('admin.blog');
    $trail->push(__('general.New Article'), route('admin.createArticle'));
});

Breadcrumbs::for('admin.showArticle', function ($trail, $article) {
    $trail->parent('admin.blog');
    $trail->push(__('general.Article'), route('admin.showArticle', $article->post_title ?? ''));
});

Breadcrumbs::for('admin.editArticle', function ($trail, $article) {
    $trail->parent('admin.blog');
    $trail->push(__('general.Edit article'), route('admin.editArticle', $article->post_title ?? ''));
});

//Blog > Manage Category
Breadcrumbs::for('admin.createCategory', function ($trail) {
    $trail->parent('admin.blog');
    $trail->push(__('general.Manage categories'), route('admin.createCategory'));
});

//Blog > Edit Category
Breadcrumbs::for('admin.editCategory', function ($trail, $category) {
    $trail->parent('admin.blog');
    $trail->push(__('general.Edit Category'), route('admin.editCategory', $category->category_name ?? ''));
});


// Dashboard > authors
Breadcrumbs::for('admin.authors.index', function ($trail) {
//    $trail->parent('dashboard');
    $trail->push(__('general.Authors'), route('admin.authors.index'));
});

// Dashboard > authors > create
Breadcrumbs::for('admin.authors.create', function ($trail) {
    $trail->parent('admin.authors.index');
    $trail->push(__('general.Create Author'), route('admin.authors.create'));
});

// Dashboard > authors > edit
Breadcrumbs::for('admin.authors.edit', function ($trail, $instructor) {
    $trail->parent('admin.authors.index');
    $trail->push(__('general.Edit Author'), route('admin.authors.edit', 1));
});