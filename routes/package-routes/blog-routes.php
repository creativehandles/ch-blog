<?php

Route::group(['name' => 'blog', 'groupName' => 'blog'], function () {
    Route::get('/blog', 'PluginsControllers\BlogController@index')->name('blog');
    //article
    Route::get('/createArticle',
        'PluginsControllers\BlogController@createArticle')->name('createArticle');
    Route::post('/createArticle',
        'PluginsControllers\BlogController@storeArticle')->name('createNewArticle');
    Route::get('/articles', 'PluginsControllers\BlogController@articles')->name('listArticles');
    Route::get('/articles/search',
        'PluginsControllers\BlogController@searchArticle')->name('ajaxSearchArticle');
    Route::get('/articles/{articleSlug}',
        'PluginsControllers\BlogController@showArticle')->name('showArticle');
    Route::get('/articles/edit/{articleId}',
        'PluginsControllers\BlogController@editArticle')->name('editArticle');
    Route::put('/articles/{articleId}',
        'PluginsControllers\BlogController@updateArticle')->name('updateArticle');
    Route::delete('/articles/delete/{articleId}',
        'PluginsControllers\BlogController@deleteArticle')->name('deleteArticle');
    Route::post('/storeBlogPostImages',
        'PluginsControllers\BlogController@storeImage')->name('storeImage');
});

Route::group(['name' => 'blogCategory', 'groupName' => 'blogCategory'], function () {
    //category
    Route::get('/createCategory',
        'PluginsControllers\BlogController@createCategory')->name('createCategory');
    Route::post('/createCategory',
        'PluginsControllers\BlogController@storeCategory')->name('createNewCategory');
    Route::get('/category/edit/{categoryId}',
        'PluginsControllers\BlogController@editCategory')->name('editCategory');
    Route::post('/category/{categoryId}',
        'PluginsControllers\BlogController@updateCategory')->name('updateCategory');
    Route::delete('/category/{categoryId}',
        'PluginsControllers\BlogController@deleteCategory')->name('deleteCategory');
    Route::get('/get-ajax-categories',
        'PluginsControllers\BlogController@getAjaxCategories')->name('getAjaxCategories');

});

//blog author related
Route::group(['name' => 'authors', 'groupName' => 'authors'], function () {
    Route::get('/authors/grid', 'PluginsControllers\AuthorsController@grid')->name('authorGrid');
    Route::resource('/authors', 'PluginsControllers\AuthorsController');
});