<?php

namespace Creativehandles\ChBlog\Http\Controllers\APIControllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\LabelResource;
use App\Http\Resources\PostResource;
use Creativehandles\ChBlog\Plugins\Blog\Blog;
use Creativehandles\ChBlog\Plugins\Blog\Repositories\BlogArticleRepository;
use Creativehandles\ChBlog\Plugins\Blog\Resources\AuthorResource;
use App\Repositories\CoreRepositories\UserRepository;
use App\Services\ExternalApiService;
use Illuminate\Http\Request;

class BlogAPIController extends Controller
{
    /**
     * @var ExternalApiService
     */
    private $apiService;
    /**
     * @var Blog
     */
    private $blog;
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(Request $request, ExternalApiService $apiService, Blog $blog, UserRepository $userRepository)
    {
        if ($request->headers->has('client-locale')) {
            app()->setLocale($request->headers->get('client-locale'));
        }

        $this->apiService = $apiService;
        $this->blog = $blog;
        $this->userRepository = $userRepository;
    }

    /**
     * Get all blog posts
     *
     * @param  Request  $request
     * @bodyParam with[0] array relationships ["tags"] Example: tags
     * @bodyParam with[1] array relationships ["categories"] Example: categories
     * @queryParam visibility int optional visibility flag. Example:1
     * @queryParam limit int optional limit record. Example:5
     * @queryParam skip int optional skip first elements. Example:2
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getAllPosts(Request $request)
    {
        $relations = $request->get('with', []);
        $visibility = $request->get('visibility', null);
        $limit = $request->get('limit', 10);
        $skip = $request->get('skip', 0);

        $posts = $this->blog->getAllArticlesWithouPagination();

        if ($visibility == '1') {
            $posts = $posts->filter(function ($item) {
                return $item->post_visibility == 1;
            });
        }

        if ($visibility == '0') {
            $posts = $posts->filter(function ($item) {
                return $item->post_visibility == 2;
            });
        }

        if ($limit) {
            $posts = $posts->slice($skip,$limit);
        }

        if ($request->headers->has('client-locale')) {
            $locale = $request->headers->get('client-locale');

            $posts = $posts->filter(function ($item) use ($locale) {
                return $item->post_language === $locale;
            });
        }


        return $this->apiService->processResponse($posts, PostResource::class, true, $relations);
    }

    /**
     * Get a blog post
     * 
     * @param  Request  $request
     * @param $id
     * @bodyParam with[0] array relationships ["tags"] Example: tags
     * @bodyParam with[1] array relationships ["categories"] Example: categories
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getPost(Request $request, $id)
    {
        $relations = $request->get('with', []);

        $locale = $request->headers->has('client-locale') ? $request->headers->get('client-locale') : null;

        if (! empty($locale)) {
            $post = $this->blog->getArticleByIdAndLocale($id, $locale);
        } else {
            $post = $this->blog->getArticleById($id);
        }

        return $this->apiService->processResponse($post, PostResource::class, false, $relations);
    }

    /**
     * @param  Request  $request
     * @bodyParam with[0] array relationships ["articles"] Example: articles
     * @queryParam visibility int optional visibility flag. Example:1
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getAllCategories(Request $request)
    {
        $relations = $request->get('with', []);
        $data = $this->blog->getAllCategories();

        return $this->apiService->processResponse($data, CategoryResource::class, true, $relations);
    }

    /**
     * @param $id
     * @param  Request  $request
     * @bodyParam with[0] array relationships ["articles"] Example: articles
     * @queryParam visibility int optional visibility flag. Example:1
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getCategory($id, Request $request)
    {
        $relations = $request->get('with', []);
        $data = $this->blog->getCategoryById($id);

        return $this->apiService->processResponse($data, CategoryResource::class, false, $relations);
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getAllTags(Request $request)
    {
        $relations = $request->get('with', []);
        $data = $this->blog->getAllTags();

        return $this->apiService->processResponse($data, LabelResource::class, true, $relations);
    }


    /**
     * @param $id
     * @param  Request  $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getTag($id, Request $request)
    {
        $relations = $request->get('with', []);
        $data = $this->blog->getTag($id);

        return $this->apiService->processResponse($data, LabelResource::class, false, $relations);
    }

    /**
     * @param $tag
     * @param  Request  $request
     * @urlParam tag required The tag id. Example: 22
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getPostsByTags($tag, Request $request)
    {
        $relations = $request->get('with', []);
        $tag = $this->blog->getTag($tag);
        //get pages for tags

        if ($tag && $relation = $tag->relation) {

            $pageIds = $tag->relation()->where('model', "BlogArticle")->get()->pluck('model_id')->all();

            $posts = app(BlogArticleRepository::class)->getWhereIn('id', $pageIds);

            return $this->apiService->processResponse($posts, PostResource::class, true, $relations);
        }

        return $this->apiService->success([], 200);
    }


    /**
     * @param $category
     * @param  Request  $request
     * @urlParam category required The category id. Example:1
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getPostsByCategory($category, Request $request)
    {
        $relations = $request->get('with', []);
        $category = $this->blog->getCategoryById($category);

        $posts = $category ? $category->articles : null;

        return $this->apiService->processResponse($posts, PostResource::class, true, $relations);
    }


    /**
     * @param  Request  $request
     * @bodyParam with[0] array relationships ["articles"] Example: articles
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getAllAuthors(Request $request)
    {
        $relations = $request->get('with', []);
        $authors = $this->userRepository->getAuthors();

        return $this->apiService->processResponse($authors, AuthorResource::class, true, $relations);
    }

    /**
     * @param  Request  $request
     * @bodyParam with[0] array relationships ["articles"] Example: articles
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getAuthor($id,Request $request)
    {
        $relations = $request->get('with', []);
        $authors = $this->userRepository->find($id);

        return $this->apiService->processResponse($authors, AuthorResource::class, false, $relations);
    }





}
