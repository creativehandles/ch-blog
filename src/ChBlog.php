<?php

namespace Creativehandles\ChBlog;

use Illuminate\Support\Facades\URL;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class ChBlog
{
    /**
     * Build FE base URL for blog preview
     *
     * @return string
     */
    public function buildFeBaseUrl()
    {
        if (! empty(config('ch-blog.fe_base_url')) && ! empty(config('ch-blog.fe_blog_url'))) {
            return config('ch-blog.fe_base_url');
        } else {
            return URL::to('/');
        }
    }

    /**
     * Build blog preview URL
     *
     * @param string $locale
     * @param int $pageId
     * @param string $pageSlug
     * @return string
     */
    public function buildPreviewUrl(string $locale = '', int $pageId = 1, string $pageSlug = '')
    {

        if (! empty(config('ch-blog.fe_base_url')) && ! empty(config('ch-blog.fe_blog_url'))) {
            return config('ch-blog.fe_base_url') . '/' . str_replace([! empty($locale)? 'lang' : 'lang/', 'id', ! empty($pageSlug)? 'slug' : '/slug'], [$locale, $pageId, $pageSlug], config('ch-blog.fe_blog_url'));
        } else {
            return LaravelLocalization::getNonLocalizedURL(route('blog-preview-article', ['id' => $pageId, 'slug' => $pageSlug]));
        }
    }
}
