<?php
/**
 * Created by PhpStorm.
 * User: deemantha
 * Date: 2/5/19
 * Time: 7:50 PM
 */

namespace Creativehandles\ChBlog\Plugins\Blog\Repositories;


use App\Repositories\BaseEloquentRepository;
use Creativehandles\ChBlog\Plugins\Blog\Models\BlogCategory;

class BlogCategoryRepository extends BaseEloquentRepository
{
    /**
     * Get the model to be used for the repository;
     *
     * @return BlogCategory
     */
    public function getModel()
    {
        return new BlogCategory();
    }

    /**
     * Update or create a page category instance
     *
     * @param array $input
     * @param string $key
     * @param bool $toArray
     *
     * @return Model|array|bool
     * @throws EloquentRepositoryException
     */
    public function updateOrCreate($input, $key = 'id', $toArray = false)
    {
        if (!is_array($input) || empty($input)) {
            return false;
        }

        if (! empty($input[$key])) {
            if (in_array($key, $this->model->translatedAttributes, true)) {
                $locale = $key !== 'category_slug' ? app()->getLocale() : null;
                $pageCategory = get_class($this->model)::whereTranslation($key, $input[$key], $locale)->first();
                $pageCategory = $pageCategory ?: $this->getModel();
            } else {
                $pageCategory = $this->model->firstOrNew([$key => $input[$key]]);
            }
        } else {
            $pageCategory = $this->getModel();
        }

        $locale = $input['locale'];
        $pageCategory->parent_id = $input['parent_id'];
        $pageCategoryTranslatable = $pageCategory->translateOrNew($locale);

        unset($input['parent_id'], $input['locale']);

        $pageCategoryTranslatable->fill($input);
        $pageCategory->save();
        $pageCategory->setDefaultLocale($locale);

        return $pageCategory;
    }
}