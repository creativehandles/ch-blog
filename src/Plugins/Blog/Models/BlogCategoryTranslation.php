<?php

namespace Creativehandles\ChBlog\Plugins\Blog\Models;

use Illuminate\Database\Eloquent\Model;

class BlogCategoryTranslation extends Model
{
    protected $table = 'blog_category_translations';
    public $fillable = ['category_name', 'category_slug', 'category_description', 'seo_title', 'seo_url', 'seo_description'];
}
