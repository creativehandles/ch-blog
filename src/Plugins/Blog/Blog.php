<?php
/**
 * Created by PhpStorm.
 * User: deemantha
 * Date: 30/4/19
 * Time: 6:45 PM
 */

namespace Creativehandles\ChBlog\Plugins\Blog;


use Creativehandles\ChBlog\Plugins\Blog\Repositories\BlogArticleRepository;
use Creativehandles\ChBlog\Plugins\Blog\Repositories\BlogCategoryRepository;
use Creativehandles\ChLabels\Plugins\Labels\Repositories\LabelRepository;
use Creativehandles\ChBlog\Plugins\Plugin;

class Blog extends Plugin
{

    /**
     * @var BlogArticleRepository
     */
    private $articleRepository;
    /**
     * @var BlogCategoryRepository
     */
    private $categoryRepository;

    //post visibility constants
    const VISIBLE_TO_ALL = 1;
    const HIDDEN_TO_PUBLIC = 2;
//    const HIDDEN_TO_ALL = 3;

    /**
     * @var LabelRepository
     */
    private $labelRepository;

    /**
     * Blog constructor.
     * @param BlogArticleRepository $articleRepository
     * @param BlogCategoryRepository $categoryRepository
     * @param LabelRepository $labelRepository
     */
    public function __construct(
        BlogArticleRepository $articleRepository,
        BlogCategoryRepository $categoryRepository,
        LabelRepository $labelRepository
    ) {
        parent::__construct();

        $this->articleRepository = $articleRepository;
        $this->categoryRepository = $categoryRepository;
        $this->labelRepository = $labelRepository;
    }


    /**
     * Category related functions start
     */


    /**
     * update/create new category in DB
     * @param $data
     * @return bool
     */
    public function storeNewCategory($data)
    {
        return $this->categoryRepository->updateOrCreate($data, 'category_name');
    }


    public function getAllCategories()
    {
        return $this->categoryRepository->all();
    }

    public function getAllParentCategories()
    {
        return $this->categoryRepository->all();
    }

    public function getCategoryById($id)
    {
        return $this->categoryRepository->find($id);
    }

    public function updateCategory($data)
    {
        return $this->categoryRepository->updateOrCreate($data, 'id');
    }

    public function deleteCategory($id)
    {
        return $this->categoryRepository->deleteById($id);
    }


    /**
     * Article Related functions start
     */

    /**
     * store new article in DB
     * @param $data
     * @return mixed
     */
    public function storeNewArticle($data)
    {
        return $this->articleRepository->updateOrCreate($data,'id');
    }

    public function updateArticle($data)
    {
        return $this->articleRepository->updateOrCreate($data, 'id');
    }

    public function getAllArticles($limit=18)
    {
        return $this->articleRepository->allWithPagination($limit=18);
    }

    public function getAllArticlesWithouPagination()
    {
        return $this->articleRepository->allWithoutPagination();
    }

    public function getNextkey()
    {
        return $this->articleRepository->getNextkey();
    }

    public function getRelatedSlugs($title)
    {
        return $this->articleRepository->findByLike('post_slug', $title)->pluck('post_slug');
    }

    public function getArticleBySlug($slug,$with = ['tags', 'categories', 'author'])
    {
        return $this->articleRepository->findBy('post_slug',$slug,$with);
    }

    public function getArticleById($id)
    {
        return $this->articleRepository->find($id, ['tags', 'categories', 'author']);
    }

    public function getArticleByIdAndLocale($id, $locale)
    {
        return $this->articleRepository->getArticleByIdAndLocale($id, $locale);
    }

    public function getArticleByName($name)
    {
        return $this->articleRepository->findByLike('post_title', '%' . $name . '%');
    }

    public function getArticlesByVisibility($visibility=true)
    {
        return $this->articleRepository->findBy('post_visibility',(int) $visibility,['tags','categories','author']);
    }

    public function deleteArticleById($id)
    {
        return $this->articleRepository->deleteById($id);
    }

    public function getRecentArticlesExceptReadingOne(
        $field,
        $condition,
        $value,
        $limit,
        $with = ['tags', 'categories', 'author']
    ) {
        return $this->articleRepository->getRecentArticlesExceptReadingOne('updated_at', 'desc', $with, $limit, $field,
            $condition, $value);
    }


    public function getRecentArticles($limit, $with = [])
    {
        return $this->articleRepository->getRecentArticles('created_at', $limit, $with);
    }
    /**
     * Tag related functions start
     */


    /**
     *
     * create new tag in DB
     * @param $data
     * @param $condition
     * @return bool
     */
    public function createNewTag($data,$condition)
    {
        return $this->labelRepository->updateOrCreateByMultipleKeys($condition,$data);
    }

    public function getAllTags($model=null)
    {
        if($model){
            return $this->labelRepository->getBy('model',$model);
        }

        return $this->labelRepository->all();
    }

    public function getTag($id)
    {
        return $this->labelRepository->find($id);
    }

}