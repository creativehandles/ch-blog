<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogCategoryTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create the translations table
        if (! Schema::hasTable('blog_category_translations')) {
            Schema::create('blog_category_translations', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('blog_category_id');
                $table->string('locale', 10)->index();
                $table->string('category_name', 191)->nullable()->default(null);
                $table->string('category_slug', 191)->nullable()->default(null);
                $table->longText('category_description')->nullable()->default(null);
                $table->string('seo_title', 191)->nullable()->default(null);
                $table->text('seo_url')->nullable()->default(null);
                $table->longText('seo_description')->nullable()->default(null);
                $table->timestamps();

                $table->unique(['blog_category_id', 'locale']);
                $table->foreign('blog_category_id')->references('id')->on('blog_category')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_category_translations');
    }
}
