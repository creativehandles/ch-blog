<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_article', function (Blueprint $table) {
            $table->increments('id');
            $table->string('post_title');
            $table->string('post_slug');
            $table->text('post_meta_description')->nullable();
            $table->string('post_language')->default('en');
            $table->integer('post_visibility')->default(1);
            $table->longText('post_body')->nullable();
            $table->integer('post_author')->unsigned();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_article');
    }
}
