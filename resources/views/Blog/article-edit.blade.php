@extends('Admin.layout')

@section("styles")
    <link rel="stylesheet" type="text/css" href="{{ asset("vendors/css/ui/dragula.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("vendors/css/extensions/toastr.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ asset("css/plugins/extensions/toastr.css") }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet"/>

@endsection

@section('content')
    @include('Admin.partials.breadcumbs',['header' => $article->post_title,'params' => $article])


    <div class="content-body">
        <div class="row">
            <div class="col-md-12 form-card">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form-center">{{__('blog.Edit article')}}</h4>

                        <div class="heading-elements">
                            <ul class="list-inline mb-0 ">
                                @if($article->isParent())
                                    <li><a class="btn btn-primary pull-right"
                                           href="{{route('admin.createArticle',['translate' => true,'parent_article'=>$article->id])}}"
                                           target="_blank">Translate</a></li>
                                @else
                                    <li><a class="btn btn-primary pull-right"
                                           href="{{route('admin.createArticle',['translate' => true,'parent_article'=>$article->parentLangArticle->id])}}"
                                           target="_blank">Translate</a></li>
                                @endif
                            </ul>
                        </div>
                        <hr>
                        <div>
                          Current Language : {{strtoupper($article->post_language)}}

                        @if($article->siblingLanguageArticles()->count() > 0)
                                <ul class="list-inline">
                                    <li>
                                        Available Translations :
                                    </li>
                                    @foreach($article->siblingLanguageArticles() as $translated)
                                        <li>
                                            @if($article->isParent())
                                                <a class="btn btn-primary "
                                                   href="{{route('admin.editArticle',['articleId' => $translated->id])}}"
                                                   target="_blank">{{strtoupper($translated->post_language)}}</a>
                                            @else
                                                <a class="btn btn-primary "
                                                   href="{{route('admin.editArticle',['articleId' => $translated->id])}}"
                                                   target="_blank">{{strtoupper($translated->post_language)}}</a>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form action="{{ route("admin.updateArticle",['articleId' => $article->id]) }}" method="post"
                                  id="postForm" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-body">

                                            {{csrf_field()}}
                                            {{method_field('PUT')}}
                                            {{--title--}}
                                            <div class="form-group">
                                                <label>{{__('blog.Post Title')}}</label>
                                                <input required type="text" class="form-control"
                                                       placeholder="Post Title"
                                                       name="title" value="{{$article->post_title}}">
                                            </div>

                                            <div class="form-group ">
                                                <label for="authorList">{{__('blog.Author')}}</label>
                                                <select required class="select2 form-control" id="authorList"
                                                        name="author">
                                                    <option value="" disabled selected> {{__('blog.Select Author')}} </option>
                                                    @foreach($authors as $author)
                                                        <option value="{{$author->id}}" {{ ($article->post_author == $author->id) ? 'selected' : ''}}>
                                                            <span>{{ $author->first_name .' ' . $author->last_name .' - '}}</span> {{ $author->user_rank }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>{{__('blog.Post URL')}}
                                                    <a title="show preview" id="show-preview" class="show-url-preview"
                                                       href="{{ \Creativehandles\ChBlog\ChBlogFacade::buildPreviewUrl('', $article->id, $article->post_slug) }}"
                                                       target="_blank">
                                                        <i class="fa fa-external-link-square"></i>
                                                    </a>
                                                </label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="span-url">{{ \Creativehandles\ChBlog\ChBlogFacade::buildPreviewUrl('', $article->id, '') }}/</span>
                                                    </div>
                                                    <input required type="text" class="form-control" id="post_slug"
                                                           placeholder="Post URL"
                                                           name="post_slug" value="{{$article->post_slug}}">
                                                </div>

                                            </div>
                                            {{--meta-title--}}
                                            <div class="form-group">
                                                <label>{{__('blog.Meta Title')}}</label>
                                                <input type="text" class="form-control"
                                                       placeholder="Meta Title"
                                                       name="meta_title" value="{{$article->post_meta_title}}">
                                            </div>


                                            {{--meta description--}}
                                            <div class="form-group">
                                                <label>{{__('blog.Meta description')}}</label>
                                                <textarea  type="text" class="form-control"
                                                          placeholder="Meta description"
                                                          name="meta_description"
                                                          rows="3">{{$article->post_meta_description}}</textarea>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>{{__('blog.Post date')}}</label>
                                                    <input type="datetime" name="post_date" class="form-control"
                                                           value="{{($article->post_date)? Carbon\Carbon::parse($article->post_date)->format('Y-m-d\TH:i') : Carbon\Carbon::parse($article->created_at)->format('Y-m-d\TH:i') }}"
                                                           placeholder="Article Date">
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{__('blog.Read time')}}</label>
                                                        <input type="text" name="post_read_time" class="form-control"
                                                               value="{{$article->post_read_time}}"
                                                               placeholder="Estimated article read time">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">

                                                </div>
                                            </div>

                                            {{--featured type--}}
                                            <div class="form-group">
                                                <label>{{__('blog.Article Featured Media Type')}}</label>
                                                <fieldset class="radio">
                                                    <label>
                                                        <input type="radio"
                                                               {{ ($article->post_feature_media_type == 'img')? 'checked' : '' }} required
                                                               name="feature_type" value="img" class="feature_type">
                                                        {{__('blog.Image')}}
                                                    </label>
                                                </fieldset>
                                                <fieldset class="radio">
                                                    <label>
                                                        <input type="radio" required
                                                               {{ ($article->post_feature_media_type == 'vdo')? 'checked' : '' }} name="feature_type"
                                                               value="vdo" class="feature_type">
                                                        {{__('blog.Video')}}
                                                    </label>
                                                </fieldset>
                                            </div>

                                            {{--featured image--}}
                                            @php
                                                $articleImagesArray = json_decode($article->post_image, true);
                                                if (is_array($articleImagesArray)) {
                                                    $img_path = $articleImagesArray[config('ch-blog.image_optimization.featured_image_default_size')] ?? null;
                                                }
                                                else {
                                                    $img_path = $article->post_image;
                                                }
                                            @endphp

                                            <div class="form-group hide" id="feature-img-container">
                                                <label>{{__('blog.Article Featured Image')}}</label>
                                                <input id="article_image" type="file" class="form-control"
                                                       name="article_image">
                                                <input type="hidden" class="form-control" name="article_image_old"
                                                       value="{{$img_path}}">
                                                <img id="article_image_preview" src="{{asset($img_path)}}"
                                                     alt="featured" class="height-150 img-thumbnail">
                                            </div>

                                            {{--featured video--}}
                                            <div class="form-group hide" id="feature-vdo-container">
                                                <label>{{__('blog.Article Featured Video')}}</label>
                                                <input id="" type="text" class="form-control"
                                                       placeholder="Place Youtube Video URL" name="article_video"
                                                       value="{{$article->post_video}}">
                                            </div>

                                            <div class="form-group options">
                                                <label for="categorySelector">{{__('blog.Post category')}}</label>
                                                <select class="select2 form-control" id="categoryList" multiple
                                                        name="categories[]">
                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id}}" {{(in_array($category->id,$article->categories->pluck('id')->all())) ? 'selected' : ''}}>
                                                            <span>{{($category->parent)? $category->parent->category_name.' - ':''}}</span> {{$category->category_name}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a href="#" id="newCategory"><b>{{__('blog.New category?')}}</b></a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    {{--language--}}
                                                    <div class="form-group">
                                                        <label for="language">{{__('blog.Language')}}</label>
                                                        <select required class="custom-select block" name="language"
                                                                id="language">
                                                            <option disabled>{{__('blog.Please select a value')}}</option>
                                                            @foreach(config('laravellocalization.supportedLocales') as $langs)
                                                                <option {{($article->post_language == $langs['short_name']) ? 'selected' : ''}} value="{{$langs['short_name']}}">
                                                                    {{ $langs['name'] }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    {{--article visibility--}}
                                                    <div class="form-group">
                                                        <label>{{__('blog.Post visibility')}}</label>
                                                        <select required class="custom-select block" name="visibility">
                                                            <option disabled>{{__('blog.Please select a value')}}</option>
                                                            <option {{(1 ==$article->post_visibility) ? 'selected' : ''}} value="1">
                                                                {{__('blog.Visible to all')}}
                                                            </option>
                                                            <option {{(2 ==$article->post_visibility) ? 'selected' : ''}} value="2">
                                                                {{__('blog.Hidden for public')}}
                                                            </option>

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            {{--body--}}
                                            <div class="form-group">
                                                <label>{{__('blog.Post body')}}</label>
                                                <textarea name="body" id="postBody" cols="30" rows="10"
                                                          placeholder="body"
                                                          class="form-control summernote-body">{{$article->post_body}}</textarea>
                                            </div>
                                            {{--link to categories--}}
                                            <div class="form-group">
                                                <label>{{__('blog.Post Tags')}}</label>
                                                <select class=" form-control" id="tagSelector"
                                                        multiple="multiple" aria-hidden="true"
                                                        name="tags[]">
                                                    <?php
                                                    $selected = $article->tags->pluck('id')->all();
                                                    $tagsCollection = $tags;
                                                    $substracted = array_diff($tags->pluck('id')->all(),$selected);
                                                    $mergedTags = array_merge($selected,$substracted);
                                                    $newtags = $tagsCollection->sortBy(function($model) use ($mergedTags){
                                                        return array_search($model->getKey(), $mergedTags);
                                                    });
                                                    ?>

                                                    @foreach($newtags as $tag)
                                                        <option {{(in_array($tag->id,$article->tags->pluck('id')->all())) ? 'selected' : ''}}  value="{{$tag->label}}">{{$tag->label}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-actions left">
                                            <button type="reset" class="btn btn-warning mr-1">
                                                <i class="ft-x"></i> {{__('blog.Clear')}}
                                            </button>
                                            <a href="{{ URL::previous() }}"><button type="button" href="" class="btn btn-warning mr-1">
                                                    <i class="ft-arrow-left"></i> {{__('blog.Go Back')}}
                                                </button></a>
                                            <button type="button" class="btn btn-primary" id="saveForm">
                                                <i class="fa fa-check-square-o"></i> {{__('blog.Save')}}
                                            </button>
                                            {{--<button type="submit" class="btn btn-primary" id="saveAndGoBack">
                                                <i class="fa fa-check-square-o"></i> {{__('blog.Update and Go back')}}
                                            </button>--}}
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card hide" id="category-card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form-center">{{__('blog.Create new Category')}}</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    @include('Admin.Blog.new-category-form')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("scripts")
    <script src="{{ asset("vendors/js/extensions/sweetalert.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("vendors/js/extensions/dragula.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("vendors/js/extensions/toastr.min.js") }}" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

    <script>
        $(document).ready(function () {
            var changeSelector = '.show-url-preview';
            //app url
            var feBaseUrl = "{{ \Creativehandles\ChBlog\ChBlogFacade::buildFeBaseUrl() }}";
            var urlPreview = $(changeSelector).attr('href');

            //set url on page load
            var currLocale = $('#language').val();
            var replacedUrl = urlPreview.replace(feBaseUrl, feBaseUrl + '/' + currLocale);
            $(changeSelector).attr('href', replacedUrl);

            var spanCurUrl = $('#span-url').html();
            $('#span-url').html(spanCurUrl.replace(feBaseUrl, feBaseUrl + '/' + currLocale));

            //set url when change page locale
            $('#language').on('change', function() {
                var currLocale = $(this).val();
                urlPreview = $(changeSelector).attr('href');
                var res = urlPreview.replace(feBaseUrl, feBaseUrl + '/' + currLocale);
                //set href
                $(changeSelector).attr('href', spanCurUrl.replace(feBaseUrl, feBaseUrl + '/' + currLocale) + $('#post_slug').val());

                $('#span-url').html(spanCurUrl.replace(feBaseUrl, feBaseUrl + '/' + currLocale));
            });

            function readURL(input) {

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        // console.log(e.target.result);
                        // $("#article_image").append('<img class="height-150 img-thumbnail" src="'+e.target.result+'">')
                        $('#article_image_preview').attr('src', e.target.result);
                        $('#article_image_preview').show();
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $('#post_slug').on('keyup', function () {
                $('#post_slug').val($.slugify($(this).val()));
            });

            $("#article_image").change(function () {
                readURL(this);
            });

            var checkedFeature = $("input[name='feature_type']:checked").val();
            if (checkedFeature == 'img') {
                $('#feature-vdo-container').hide();
                $('#feature-img-container').show();
            } else {
                $('#feature-img-container').hide();
                $('#feature-vdo-container').show();
            }

            $('.feature_type').on('change', function () {
                var checkedValue = $(this).val();

                if (checkedValue == 'img') {
                    $('#feature-vdo-container').hide();
                    $('#feature-img-container').show();
                } else {
                    $('#feature-img-container').hide();
                    $('#feature-vdo-container').show();
                }
            });


            $('#category-name').on('keyup', function () {
                var cat_name = $('#category-name').val();
                $('#seo_title').val(cat_name);
                $('#category_slug').val($.slugify(cat_name));
            });

            $('#category-description').on('keyup', function () {
                $('#seo_description').val($('#category-description').val());
            });

            $('#newCategory').on('click', function (e) {
                e.preventDefault();
                $('.form-card').removeClass('col-md-12');
                $('.form-card').addClass('col-md-8');
                $('#category-card').toggle();
            });


            //checkbox require  validation
            function checkboxRequired() {
                var requiredCheckboxes = $('.options :checkbox[required]');

                if (requiredCheckboxes.is(':checked')) {
                    requiredCheckboxes.removeAttr('required');
                } else {
                    requiredCheckboxes.attr('required', 'required');
                }

                requiredCheckboxes.change(function () {
                    if (requiredCheckboxes.is(':checked')) {
                        requiredCheckboxes.removeAttr('required');
                    } else {
                        requiredCheckboxes.attr('required', 'required');
                    }
                });
            };

            checkboxRequired();

            //new category form submission and append added category to article form
            var categoryForm = $('#category-form');

            categoryForm.on('submit', function (e) {
                e.preventDefault();

                $.ajax({
                    data: categoryForm.serialize(),
                    dataType: 'json',
                    method: 'post',
                    url: "{{ route("admin.createNewCategory") }}",
                }).done(function (response) {
                    toastr.success('New category added', 'Success');
                    var data = response.data;

                    $('#categoryList').last().append(
                        `<option selected value="` + data.id + `">` + ((data.parent) ? data.parent + ' - ' : '') + data.category_name + `</option>`
                    );

                    $('.options :checkbox[required]').removeAttr('required');
                    categoryForm[0].reset();

                }).fail(function (error) {
                    var messages = error.responseJSON.msg;

                    for (message in messages) {
                        toastr.error(messages[message], 'Error', {timeOut: 5000});
                    }
                });
            });

            //select 2
            $('#parentCategory').select2({
                placeholder: 'Select a parent category',
                closeOnSelect: true,
                allowClear: true,
            });

            //select 2 tag initiator
            $('#tagSelector').select2({
                placeholder: 'Select tags',
                tags: true
            });

            $('#categoryList').select2({
                placeholder: 'Select a category',
                closeOnSelect: true,
                allowClear: true,
            });

            //select2 initiator
            $('#authorList').select2({
                placeholder: 'Select an author',
                closeOnSelect: true,
                allowClear: true,
            });

            //select2 initiator
            $('#categorySelector').select2({
                placeholder: 'Select categories',
                minimumResultsForSearch: Infinity,
            });


            var saveFormBtn = $('#saveForm');
            var saveBtnOriginal = saveFormBtn.html();

            $('#saveForm').on('click', function (e) {
                e.preventDefault();
                saveFormBtn.html('<i class="fa fa-spinner"></i> Processing');

                //new article form submission
                var postForm = $('#postForm')[0];

                // Create an FormData object
                var data = new FormData(postForm);
                $.ajax({
                    data: data,
                    dataType: 'json',
                    method: 'post',
                    processData: false,  // Important!
                    contentType: false,
                    url: "{{ route("admin.updateArticle",['articleId' => $article->id]) }}",
                }).done(function (response) {
                    saveFormBtn.html(saveBtnOriginal);
                    toastr.success('Article Updated', 'Success', {
                        timeOut: 5000,
                        fadeOut: 1000,
                        progressBar: true,
                    });
                }).fail(function (error) {
                    saveFormBtn.html(saveBtnOriginal);
                    var messages = error.responseJSON.msg;
                    for (message in messages) {
                        toastr.error(messages[message], 'Error',{ timeOut: 5000  });
                    }
                });
            });

            //form reset button functionality
            $("button[type='reset']").on("click", function (event) {
                event.preventDefault();
                postForm[0].reset();
                $('#postBody').summernote("reset");
                $('#tagSelector').val('').trigger('change');
            });
        });
    </script>
@endsection
